import { uid, pool } from './util';


export default class Quizz {
	constructor({ id = uid(), courseId, title }) {
		this.id = id;
		this.courseId = courseId;
		this.title = title;
	}

	getQuestions() {
		return pool.questions
			.filter(q => q.quizzId === this.id)
			.sort((a, b) => a.order - b.order);
	}
}
