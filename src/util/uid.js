const crypto = require('crypto');

export default (len = 12) => crypto.randomBytes(len).toString('base64')
	.replace(/=+$/, '')
	.replace(/\+/g, '-')
	.replace(/\//g, '_');
