const { Map: IMap } = require('immutable');

let __cache = new Map();

export default () => new Proxy(Object.create(null), {
	get(_, name) {
		if (name === '__cache') {
			return __cache; // expose, to feed it with test data
		}
		return __cache.get(name) || __cache.set(name, IMap()).get(name);
	},
	set(_, k, v) {
		if (k === '__cache') {
			__cache = v;
			return;
		}
		return __cache.set(k, v);
	}
});
