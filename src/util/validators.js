const assert = (bool, message) => {
	if (!bool) {
		throw new TypeError(message);
	}
};

export const questionValidator = ({
	solution,
	title,
	choices,
} = {}) => {
	assert(typeof title === 'string', `title must be a string, you provided ${title}`);
	assert(Array.isArray(choices), `choices must be an array, you provided ${choices}`);
	assert(Number.isInteger(solution), `solution must be a number, you provided ${solution}`);
	assert(solution >= 0 && solution <= choices.length - 1, `solution must be in [0, choices.length-1], you provided ${solution}`);
};

