import createPool from './createPool';

export * from './validators';

export { default as uid } from './uid';

/**
 * share pool instance
 */
export const pool = createPool();

// to sort an array of arrays
export const orderComparator = (a, b) => {
	for (let i = 0; i < Math.max(a.length, b.length); i++) {
		const x = a[i] || 0, y = b[i] || 0;
		if (x !== y) return x - y;
	}
	return 0;
};
