import { readFileSync } from 'fs';
import { graphql, buildSchema } from 'graphql';
import { pool } from './index';

const schemaString = readFileSync(__dirname + '/schema.graphql').toString();

const schema = buildSchema(schemaString);

const rootValue = {
	students({ first, after, orderBy, courseId, teacherId }) {
		let students = pool.students;
		if (orderBy) {
			// students = students.sort((a, b) => String(a).localeCompare(b));
		}
		if (after) {
			// ..
		}
		if (courseId) {
			students = students.filter(s => s.courseId === courseId);
		}
		if (teacherId) {
			// ...
		}
		return [...students.slice(0, first).valueSeq()];
	}
};

export default (source, variableValues) => graphql({
	schema,
	rootValue,
	source,
	variableValues,
});
