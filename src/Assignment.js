import { uid, pool } from './util';
import Quizz from './Quizz';

export default class Assignment {
	constructor({ id = uid(), studentId, quizzId, created = new Date() }) {
		this.id = id;
		this.studentId = studentId;
		this.quizzId = quizzId;
		this.created = created;
	}

	getSubmissions() {
		return pool.submissions
			.filter(q => q.assignmentId === this.id);
	}

	getQuizz() {
		return new Quizz(pool.quizzs.get(this.quizzId));
	}
}
