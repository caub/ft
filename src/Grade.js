export default class Grade {
	constructor({ results, fromDate, toDate, courseId }) {
		this.fromDate = fromDate; // optional
		this.toDate = toDate; // optional
		this.courseId = courseId; // optional
		this.results = results; // Array of objects with shape {assignmentId, grade}
		this.grade = results.reduce((total, { grade }) => total + grade, 0) / results.length;
	}
	valueOf() {
		return this.grade;
	}
	toString() {
		return `grade: ${this.grade}`;
	}
}
