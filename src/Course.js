import Quizz from './Quizz';
import { pool, uid, questionValidator } from './util';


export default class Course {
	constructor({ id = uid(), teacherId, studentIds }) {
		this.id = id;
		this.teacherId = teacherId;
		this.studentIds = studentIds;
	}

	getQuizzs() {
		return pool.quizzs
			.filter(q => q.courseId === this.id)
			.map(q => new Quizz(q));
	}
	addQuizz({ id = uid(), title, questions }) {
		const quizz = { id, courseId: this.id, title };

		questions.forEach(q => {
			questionValidator(q);
		});

		pool.quizzs = pool.quizzs.set(id, quizz);
		questions.forEach(({ id = uid(), solution, title, choices }, i) => {
			pool.questions = pool.questions.set(id, {
				id,
				solution,
				title,
				choices,
				quizzId: quizz.id,
				order: i + 1
			});
		});

		return new Quizz(quizz);
	}
}
