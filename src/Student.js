import { Map } from 'immutable';
import parse from 'date-fns/parse'
import { pool, uid } from './util';
import Grade from './Grade';
import Course from './Course';
import Quizz from './Quizz';
import Assignment from './Assignment';

export default class Student {
	constructor({ id = uid() }) {
		this.id = id;
	}
	computeGrade({ fromDate, toDate, courseId }) {
		const assignments = [...this.getAssignments({ fromDate, toDate, courseId }).values()];
		const results = assignments.map(a => {
			const quizz = pool.quizzs.get(a.quizzId) && new Quizz(pool.quizzs.get(a.quizzId));
			if (!quizz) {
				throw new Error(`missing quizz ${a.quizzId}`); // should avoid throwing there
			}
			const questions = quizz.getQuestions();

			const subs = pool.submissions.filter(s => s.assignmentId === a.id);
			const subsPerQuestion = Map(subs.valueSeq().map(s => [s.questionId, s]));

			return {
				assignmentId: a.id,
				grade: questions.reduce(
					(total, { solution }, id) => total + (solution === (subsPerQuestion.get(id) && subsPerQuestion.get(id).answer)),
					0
				) / (a.partial ? subsPerQuestion.size : questions.size)
			};
		});

		// todo assignment or quizz could have coefficients

		return new Grade({ results, fromDate, toDate, courseId });
	}

	getAssignments({ fromDate = 0, toDate = '2050', courseId } = {}) {
		const fromD = parse(fromDate);
		const toD = parse(toDate);
		const predicate = courseId ?
			a => a.studentId === this.id && a.created >= fromD && a.created <= toD && pool.quizzs.get(a.quizzId).courseId === courseId :
			a => a.studentId === this.id && a.created >= fromD && a.created <= toD;
		return pool.assignments.filter(predicate).map(a => new Assignment(a));
	}

	getPendingAssignments({ fromDate, toDate, courseId } = {}) {
		return this.getAssignments({ fromDate, toDate, courseId }).filter(a => !pool.submissions.find(s => s.assignmentId === a.id));
	}

	addSubmission({ id = uid(), assignmentId, questionId, answer, ...rest }) {
		if (!assignmentId || !questionId) {
			throw new Error('missing assignmentId or questionId'); // todo check of valid ids in DB
		}
		if (!Number.isInteger(answer)) {
			throw new Error('wrong answer format');
		}
		if (!this.getPendingAssignments().get(assignmentId)) {
			throw new Error('assignment already submitted or not yours!');
		}

		pool.submissions = pool.submissions.set(id, {
			id,
			questionId,
			assignmentId,
			answer,
			created: new Date(),
			...rest
		});
	}

	getCourses() {
		return pool.courses
			.filter(o => o.studentIds.includes(this.id))
			.map(c => new Course(c));
	}
}
