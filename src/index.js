export { default as Course } from './Course';
export { default as Grade } from './Grade';
export { default as Quizz } from './Quizz';
export { default as Student } from './Student';
export { default as Teacher } from './Teacher';
