import { pool, uid } from './util';
import Course from './Course';
import Quizz from './Quizz';
import Assignment from './Assignment';

export default class Teacher {
	constructor({ id = uid() }) {
		this.id = id;
	}
	getCourses() {
		return pool.courses.filter(o => o.teacherId === this.id).map(c => new Course(c));
	}
	getQuizzs(courseId) {
		return pool.quizzs.filter(q => q.courseId === courseId).map(q => new Quizz(q));
	}
	// addQuizz(quizz) {
	// 	const { courseId } = quizz;
	// 	const course = this.getCourses().get(courseId);
	// 	if (!course || !course.id) {
	// 		throw new Error(`course id ${quizz.courseId} not found`);
	// 	}
	// 	return new Course(course).addQuizz(quizz);
	// }
	addAssignment({ studentId, quizzId, ...rest } = {}) {
		if (!studentId || !quizzId) {
			throw new Error(`missing studentId or quizzId`);
		}
		// todo check this teacher owns this quizz?

		const a = new Assignment({ studentId, quizzId, ...rest });

		pool.assignments = pool.assignments.set(a.id, a);
		return a;
	}
}
