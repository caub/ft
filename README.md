# Spec

Please use your most proficient programming language to create object oriented design and
use test driven development to implement classes and methods with appropriate data structure
and test the code for the following scenario. Please add comments describing any assumptions
you make:
- There are Teachers
- There are Students
- Students are in classes that teachers teach
- Teachers can create multiple quizzes with many questions (each question is multiple choice)
- Teachers can assign quizzes to students
- Students solve/answer questions to complete the quiz, but they don't have to complete it at
once. (Partial submissions can be made).
- Quizzes need to get graded
- For each teacher, they can calculate each student's total grade accumulated over a semester
for their classes

## Setup

- `npm install`
- Run `npm test` to run the tests

## Todos

- [x] Add a Question class, with a quizzId, an index/order, it allows to have better stats (like seeing results of students for a given question)
- [ ] [enhancement] Use graphql (just locally, no server) to allow to combine models and filters

## Class/Object diagram (each have `id`)

WIP, it'll be replaced by [schema](src/util/schema.graphql)

- `Teacher`
  - `getCourses()`
  - `getQuizzs()`
  - `addAssignment({studentId, quizzId})`
- `Student`
  - `computeGrade({fromDate, toDate, courseId})`
  - `getAssignments({fromDate, toDate, courseId})`
  - `getPendingAssignments({fromDate, toDate, courseId})`
  - `getCourses()`
  - `addSubmission({assignmentId, questionId, answer})`
- `Course`
  - `teacherId`
  - `studentIds`
  - `getQuizzs()`
  - `addQuizz({title, questions})`
- `Quizz`
  - `courseId`
  - `getQuestions()` [Question]
- `Question`
  - `quizzId`
  - `title`
  - `choices` [Integer]
  - `solution` Integer
  - `order` Integer
- `Assignment`
  - `quizzId`
  - `studentId`
  - `partial` Boolean // allow a student to submit partially
  - `created` Date
  - `getSubmissions()` [Submission]
  - `getQuizz()` Quizz
- `Submission`
  - `questionId`
  - `assignmentId` // (assignmentId, questionId) should be unique
  - `answer` Integer
  - `created`
- `Grade`
  - `results` [{assignmentId, grade}]
  - `grade` Number
  - `fromDate`
  - `toDate`
  - `courseId`
