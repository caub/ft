import { Map, Set } from 'immutable';
import { pool } from '../src/util';

const mockData = {
	teachers: Array.from({ length: 15 }, (_, i) => ({ id: `TEACHER${i + 1}` })),

	students: Array.from({ length: 300 }, (_, i) => ({ id: `STUDENT${i + 1}` })),

	courses: Array.from({ length: 15 }, (_, i) => ({
		id: `COURSE${i + 1}`,
		teacherId: `TEACHER${i + 1}`,
		studentIds: Set(Array.from({ length: 20 }, (_, j) => `STUDENT${20 * i + j + 1}`))
	})),

	quizzs: Array.from({ length: 15 }, (_, i) => ({
		id: `QUIZZ${i + 1}`,
		courseId: `COURSE${i + 1}`,
		title: 'Quizz Foo ' + i
	})),

	questions: [].concat( // flatten questions per course
		...Array.from({ length: 15 }, (_, i) =>
			Array.from({ length: 10 + 10 * Math.random() }, (_, j) => ({
				id: `Q${i + 1}_${j + 1}`,
				quizzId: `QUIZZ${i + 1}`,
				order: i + 1,
				title: `Some question.. ${j} ?`,
				choices: [
					'Do this',
					'Do that',
					`Something else ${j}`
				],
				solution: j < 5 ? 1 : Math.floor(3 * Math.random())
			}))
		)
	),


	// each student is assigned quizz1
	assignments: Array.from({ length: 300 }, (_, j) => ({
		id: `ASG1_${j + 1}`,
		studentId: `STUDENT${j + 1}`,
		quizzId: `QUIZZ1`,
		created: new Date(2017, 10, 2)
	})),

	// make each student, answer the 10 first questions (quizz have between 10 and 19 questions)
	submissions: [].concat(
		...Array.from({ length: 300 }, (_, i) =>
			Array.from({ length: 10 }, (_, j) => ({
				id: `SUB${i + 1}_${j + 1}`,
				questionId: `Q1_${j + 1}`,
				assignmentId: `ASG1_${i + 1}`,
				answer: j < 5 ? 1 : Math.floor(3 * Math.random()),
				created: new Date(2017, 10, 6)
			}))
		)
	)
};


// feed the pool cache with fake data
export default () => {
	Object.keys(mockData).forEach(k => {
		pool.__cache.set(k, Map(mockData[k].map(o => [o.id, o])));
	});
};
