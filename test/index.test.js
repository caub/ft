import {
	Teacher,
	Student,
} from '../src';

import initMockData from './mock-data';
// import { pool } from '../src/util';

beforeAll(() => {
	initMockData();
});


it('should return student grades between 0 and 1', () => {
	const s1 = new Student({ id: 'STUDENT1' });
	const grade = s1.computeGrade({ fromDate: new Date(2017, 0, 1), toDate: new Date(2018, 0, 1) });
	expect(grade >= 0);
	expect(grade <= 1);
	console.log('grade: ' + grade);
});

it('should allow a teacher to create a new quizz', () => {
	const t1 = new Teacher({ id: 'TEACHER1' });
	const course = t1.getCourses().first();
	expect(course);

	const quizz = course.addQuizz({
		title: 'Some quizz',
		questions: [{
			title: 'Hello',
			choices: ['World', 'Space'],
			solution: 0
		}]
	});

	expect(quizz.id);
	expect(quizz.getQuestions().size).toEqual(1);
});

it('should allow a teacher to assign a quizz to a student', () => {
	const t1 = new Teacher({ id: 'TEACHER1' });
	const course = t1.getCourses().first();

	// pick the newly created quizz
	const quizz = course.getQuizzs().find(q => q.title === 'Some quizz');
	expect(quizz);

	const s1 = new Student({ id: 'STUDENT1' });

	const a = t1.addAssignment({
		studentId: s1.id,
		quizzId: quizz.id
	});
	expect(a.id);
});

it('should allow a student to submit his answers to an assignment', () => {
	const s1 = new Student({ id: 'STUDENT1' });
	const asg = s1.getPendingAssignments().first(); // take 2nd one, (the one assigned just before)
	const quizz = asg.getQuizz();
	expect(asg);
	expect(quizz);
	const questions = quizz.getQuestions();
	expect(questions.size).toEqual(1);

	s1.addSubmission({
		assignmentId: asg.id,
		questionId: questions.first().id,
		answer: 0,
	});

	const grade = s1.computeGrade({ fromDate: new Date(2017, 0, 1) });
	expect(grade >= 0 && grade <= 1);
	console.log(`${grade} got better!`);
});

it('should allow to show grades per student, and per courses', () => {
	const s1 = new Student({ id: 'STUDENT1' });

	const grade = s1.computeGrade({ fromDate: new Date(2017, 0, 1) });

	const course = s1.getCourses().first();
	expect(course);

	const gradeForCourse1 = s1.computeGrade({
		fromDate: new Date(2017, 0, 1),
		courseId: course.id
	});
	expect(+gradeForCourse1).toEqual(+grade); // since this student is only in one course
});
