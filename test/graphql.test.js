import gql from '../src/util/gql';
import initMockData from './mock-data';

beforeAll(() => {
	initMockData();
});

it('should return a list of students', async () => {
	const { data } = await gql(`{students(first: 15) {id}}`);

	expect(data.students).toHaveLength(15);
});
