import nodeGlobals from 'rollup-plugin-node-globals';

export default {
	format: 'cjs',
	plugins: [nodeGlobals()]
}
